/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pactera.ilearn.web.rest.vm;
