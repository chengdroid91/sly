## 部署到 ec2 的手顺

### 克隆代码并进入到代码目录

```
git clone https://gitlab.com/chengdroid91/sly.git
cd sly
```

### 配置环境变量

```
echo 'export USER_NAME="{人名拼音}"' >>~/.bash_profile
source ~/.bash_profile
```

### 配置 ec2 启动脚本

```
sed -i "s/{USER_NAME}/${USER_NAME}/" user_data_script
```

### 使用 aws cli 启动 ec2

```
aws ec2 run-instances --image-id ami-0218d08a1f9dac831 \
--count 1 \
--instance-type t2.small \
--key-name ilearn_key \
--security-group-ids sg-0676bc31f8652a895 \
--subnet-id subnet-07614ac488b364874 \
--iam-instance-profile Name=ilearn-ec2-profile \
--tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=${USER_NAME}}]" \
--user-data file://user_data_script
```
